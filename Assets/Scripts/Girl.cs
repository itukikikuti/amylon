﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Girl : MonoBehaviour
{
    [SerializeField] private new Rigidbody2D rigidbody = null;
    [SerializeField] private Player player = null;
    [SerializeField] private float power = 0f;
    [SerializeField] private float limit = 0f;

    private void Update()
    {
        if (transform.position.x - 5f > player.transform.position.x ||
            transform.position.x + 5f < player.transform.position.x ||
            transform.position.y + 3f < player.transform.position.y)
            return;

        if (Mathf.Abs(player.transform.position.x - transform.position.x) < 1f)
            return;

        //rigidbody.angularVelocity = Mathf.MoveTowards(rigidbody.angularVelocity, limit * -sign, power);

        var sign = Mathf.Sign(player.transform.position.x - transform.position.x);
        var x = rigidbody.velocity.x;
        var y = rigidbody.velocity.y;
        x = Mathf.Lerp(x, limit * sign, power);
        rigidbody.velocity = new Vector2(x, y);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position + new Vector3(0f, 1.5f, 0f), new Vector3(10f, 3f, 0f));
    }
}
