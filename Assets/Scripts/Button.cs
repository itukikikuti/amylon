﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    [SerializeField] private Elevator elevator = null;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(elevator.Activate());
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        StartCoroutine(elevator.UnActivate());
    }
}
