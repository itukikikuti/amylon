﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    [SerializeField] GameObject bullet;
    [SerializeField] float power;

    IEnumerator Start()
    {
        while (true)
        {
            GameObject clone = Instantiate(bullet);
            clone.transform.position = this.transform.position;
            clone.GetComponent<Rigidbody2D>().velocity = this.transform.right * power;
            Destroy(clone, 10f);

            yield return new WaitForSeconds(5f);
        }
    }
}
