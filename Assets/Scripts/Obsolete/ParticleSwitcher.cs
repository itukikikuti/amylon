﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleSwitcher : MonoBehaviour
{
    [SerializeField] float wait;

    ParticleSystem ps;

    IEnumerator Start()
    {
        ps = this.GetComponent<ParticleSystem>();

        while (true)
        {
            yield return new WaitForSeconds(wait);

            ps.Play();

            yield return new WaitForSeconds(wait);

            ps.Stop();
        }
    }
}
