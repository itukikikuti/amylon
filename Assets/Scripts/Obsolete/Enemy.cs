﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Enemy : MonoBehaviour
{
    Rigidbody2D rb;

    IEnumerator Start()
    {
        rb = GetComponent<Rigidbody2D>();

        while (true)
        {
            rb.AddForce(new Vector2(0f, 5f), ForceMode2D.Impulse);
            yield return new WaitForSeconds(3f);
        }
    }
}
