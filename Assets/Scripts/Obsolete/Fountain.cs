﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class Fountain : MonoBehaviour
{
    [SerializeField] float power;

    ParticleSystem ps;

    void Start()
    {
        ps = this.GetComponent<ParticleSystem>();
    }

    void OnParticleCollision(GameObject collision)
    {
        if (ps.time < 0.1f && collision.tag == "Player")
        {
            collision.GetComponent<Player>().Release();
            collision.GetComponent<Rigidbody2D>().velocity += new Vector2(0f, power);
        }
    }
}
