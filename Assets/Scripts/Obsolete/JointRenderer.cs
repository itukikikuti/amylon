﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(AnchoredJoint2D))]
[RequireComponent(typeof(LineRenderer))]
public class JointRenderer : MonoBehaviour
{
    AnchoredJoint2D aj;
    LineRenderer lr;

    void Start()
    {
        aj = this.GetComponent<AnchoredJoint2D>();
        lr = this.GetComponent<LineRenderer>();
        lr.positionCount = 2;
    }

    void Update()
    {
        Vector3 temp = Vector3.zero;
        if (aj.connectedBody != null)
            temp = aj.connectedBody.transform.position;

        lr.SetPosition(0, this.transform.TransformPoint(new Vector3(aj.anchor.x, aj.anchor.y)));
        lr.SetPosition(1, temp + new Vector3(aj.connectedAnchor.x, aj.connectedAnchor.y));
    }
}
