﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulley : MonoBehaviour
{
    [SerializeField] float force;

    void OnParticleCollision(GameObject collision)
    {
        if (collision.tag == "Water")
        {
            this.GetComponent<Rigidbody2D>().velocity += new Vector2(0f, force);
        }
    }
}
