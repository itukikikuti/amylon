﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Player")
            return;

        PlayerPrefs.SetString("CheckPoint", JsonUtility.ToJson(this.transform.position));
        Debug.Log($"CheckPoint {this.transform.position}");
    }

    void Test()
    {
    }
}
