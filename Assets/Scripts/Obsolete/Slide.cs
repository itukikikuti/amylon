﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SliderJoint2D))]
public class Slide : MonoBehaviour
{
    [SerializeField] float wait;

    SliderJoint2D sj;

    IEnumerator Start()
    {
        sj = GetComponent<SliderJoint2D>();
        var motor = sj.motor;

        while (true)
        {
            yield return new WaitForSeconds(wait);
            motor.motorSpeed = -motor.motorSpeed;
            sj.motor = motor;

            yield return new WaitForSeconds(wait);
            motor.motorSpeed = -motor.motorSpeed;
            sj.motor = motor;
        }
    }
}
