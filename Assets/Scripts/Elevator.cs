﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    [SerializeField] private Vector3 delta = Vector3.zero;
    private Vector3 origin = Vector3.zero;

    private void Start()
    {
        origin = transform.position;
    }

    public IEnumerator Activate()
    {
        var target = origin + delta;

        while (transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, 0.01f);
            yield return new WaitForFixedUpdate();
        }
    }

    public IEnumerator UnActivate()
    {
        while (transform.position != origin)
        {
            transform.position = Vector3.MoveTowards(transform.position, origin, 0.01f);
            yield return new WaitForFixedUpdate();
        }
    }
}
