﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(SpringJoint2D))]
public class Player : MonoBehaviour
{
    Transform body;
    Rigidbody2D rigidbody;
    CircleCollider2D collider;
    SpringJoint2D joint;
    AudioSource audioSource;
    Vector3 _axis;
    Vector3 _target;
    bool _isStick = false;
    Vector3 _direction = Vector3.right;
    float _distance = 0f;
    [SerializeField] LayerMask layerMask;
    [SerializeField] Vector3 original;
    [SerializeField] float spring;
    [SerializeField] float repulsion;
    [SerializeField] float limit;
    Vector3 _force;
    [SerializeField] AnimationCurve expand;
    [SerializeField] AudioClip stickSound;
    [SerializeField] bool startFromCheckPoint;
    int touchFrame = 0;

    void Awake()
    {
        if (PlayerPrefs.HasKey("CheckPoint") && startFromCheckPoint)
        {
            this.transform.position = JsonUtility.FromJson<Vector3>(PlayerPrefs.GetString("CheckPoint"));
        }

        body = transform.GetChild(0);
        rigidbody = GetComponent<Rigidbody2D>();
        collider = GetComponent<CircleCollider2D>();
        joint = GetComponent<SpringJoint2D>();
        audioSource = this.gameObject.AddComponent<AudioSource>();
    }

    void Update()
    {
        if (joint.connectedBody == null)
            Release();

        if (Input.GetMouseButtonDown(0))
        {
            _isStick = false;
            _axis = Input.mousePosition;
            _distance = 0f;
            //wink.t = 1f;
            touchFrame = 0;
        }
        if (Input.GetMouseButton(0))
        {
            if (!_isStick)
            {
                touchFrame++;
                _target = Input.mousePosition;
                _distance = Mathf.Lerp(_distance, expand.Evaluate(Vector3.Distance(_target, _axis) / Screen.dpi) * transform.localScale.x, 0.2f);
                if (_distance > 0f)
                {
                    _direction = (_target - _axis).normalized;
                    Extend();
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            Shrink();
        }
    }

    void FixedUpdate()
    {
        _force += (original - body.localScale) * spring * Time.timeScale;
        _force *= Mathf.Lerp(1f, repulsion, Time.timeScale);
        body.localScale += _force;
        body.localScale = Vector3.Min(body.localScale, original + new Vector3(limit, limit, limit));
        body.localScale = Vector3.Max(body.localScale, original - new Vector3(limit, limit, limit));
    }

    void Extend()
    {
        float scale = transform.localScale.x;

        transform.localScale = new Vector3(1f, _direction.x <= 0f ? -1f : 1f, 1f);
        body.position = transform.position + (_direction * scale * _distance) / 2f;
        body.eulerAngles = new Vector3(0f, 0f, Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg);
        original = new Vector3(_distance + 1f, Mathf.Lerp(1f, 0.25f, _distance / 4f), 1f);

        RaycastHit2D hit = Physics2D.CircleCast(transform.position, collider.radius * 0.9f, _direction, scale * _distance, layerMask.value);
        if (hit.transform == null)
            return;

        if (hit.collider.isTrigger || hit.rigidbody == null)
        {
            Shrink();
        }
        else
        {
            Stick(hit.rigidbody, hit.centroid);
        }
    }

    public void Release()
    {
        joint.enabled = false;
        rigidbody.angularDrag = 0.05f;
    }

    void Shrink()
    {
        _isStick = true;
        _distance = 0f;
        body.localPosition = Vector3.zero;
        _force += new Vector3(-(body.localScale.x - 1f), body.localScale.x - 1f) * 0.5f;
        original = Vector3.one;
        body.localScale = Vector3.one;
    }

    void Stick(Rigidbody2D connectedBody, Vector3 hit)
    {
        audioSource.pitch = Random.Range(1f, 1.5f);
        audioSource.volume = Mathf.Clamp(_distance / 3f, 0.2f, 1f);
        audioSource.PlayOneShot(stickSound);

        joint.enabled = true;
        rigidbody.velocity = Vector2.zero;
        Vector3 position = transform.position;
        transform.position = hit;
        joint.autoConfigureConnectedAnchor = true;
        joint.connectedBody = connectedBody;
        joint.autoConfigureConnectedAnchor = false;
        joint.frequency = 50f / Time.timeScale;
        transform.position = position;
        rigidbody.angularDrag = 10f;
        Shrink();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        foreach (var contact in collision.contacts)
        {
            OnTriggerEnter2D(contact.collider);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            SceneManager.LoadScene("Scene");
        }
    }
}
